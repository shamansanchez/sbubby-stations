# sbubby

## Getting Started
Install `poetry` following their [official installation instructions](https://python-poetry.org/docs/master/#installing-with-the-official-installer). Then run the following commands from the root of this directory:

```bash
poetry install
poetry shell
```

Ensure your installation works:

```bash
sbubby
mappy
```

## How to add Stations, Switches, and Lines

Stations, Switches and Lines are stored in `inputs.yml`. Station information is stored in the `stations` key, Switch information is stored in the `switches` key, and Line information is stored in the `lines` key.

Before committing, pushing and merging changes to `inputs.yml`, please test your configuration by running the following command:

```bash
sbubby
mappy
```

If it fails, stop and ask for help in the Discord. If it succeeds and `map.png` looks like you were expecting, you're good to go!

### lines
A Line is a collection of Stations.

`lines` is a dictionary of Lines. Each key is the name of a Line.

#### lines.\<line\>.color
A RGB-coded color spec. Please describe your color with a YAML comment next to the spec.

Example:
```
rgb(255, 215, 0) # gold
```

#### lines.\<line\>.name
A name for your Line. Currently this field is not used.

### stations
![A Station](static/station.png)
A Station is a stopping point in the Sbubby rail system. They are referenced when setting your destination via the in-game command:

```
/ticket <station>
```

`stations` is a dictionary of Stations. Each key is the name of a Station.

#### stations.\<station\>.connections
A list of connected Stations and/or Switches. This list must contain 1 or 2 connected Stations and/or Switches, no more, no less.

#### stations.\<station\>.coordinates
Each Station should have a post composed of two blocks of any type stacked vertically. The coordinates of the upper block of the two are what needs to be referenced in this spec. See the above screenshot for an example of how to build your Stations.

##### stations.\<station\>.coordinates.x
The "X" coordinate of the upper block in the Station's post.

##### stations.\<station\>.coordinates.y
The "Y" coordinate of the upper block in the Station's post.

##### stations.\<station\>.coordinates.z
The "Z" coordinate of the upper block in the Station's post.

#### stations.\<station\>.line
A reference to the `line` that this Station is a part of.

### switches
![A Switch](static/switch.png)
A Switch is a junction between several Stations and/or Switches. Unlike Stations, Switches are not members of Lines.

`switches` is a dictionary of Switches. Each key is the name of a Switch.

#### switches.\<switch\>.connections
A dictionary of connected Stations and/or Switches. This dictionary must contain between 1-4 elements.

##### switches.\<switch\>.connections.\<connection\>
A dictionary of connection details. The name of this dictionary should match the name of a Station or Switch connected to this Switch.

###### switches.\<switch\>.connections.\<connection\>.dir
A character describing the direction of the connection relative to its Switch. Valid values are "N", "E", "S", and "W".

##### switches.\<switch\>.coordinates
Each Switch should have a ring of rails surrounding a single Redstone Torch. The coordinates of the Redstone Torch are what need to be referenced in this spec. See the above screenshot for an example of how to build your Switches.

###### switches.\<switch\>.coordinates.x
The "X" coordinate of the Redstone Torch.

###### switches.\<switch\>.coordinates.y
The "Y" coordinate of the Redstone Torch.

###### switches.\<switch\>.coordinates.z
The "Z" coordinate of the Redstone Torch.
