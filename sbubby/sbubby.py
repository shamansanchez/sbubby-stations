import math
import pprint
import sys

import networkx as nx
import yaml

from sbubby import graphy

# Unpack graph and its metadata from graph generator
g, stations, switches = graphy.get_graph()
all_coordinates = nx.get_node_attributes(g, "coordinates")
station_coordinates = nx.get_node_attributes(stations, "coordinates")
switch_coordinates = nx.get_node_attributes(switches, "coordinates")


def weight(u, v, d):
    """
    Function used to calculate distance between two stops
    """
    ux = all_coordinates[u]["x"]
    uy = all_coordinates[u]["y"]
    uz = all_coordinates[u]["z"]

    vx = all_coordinates[v]["x"]
    vy = all_coordinates[v]["y"]
    vz = all_coordinates[v]["z"]

    w = math.sqrt(pow(vx - ux, 2) + pow(vy - uy, 2) + pow(vz - uz, 2))
    return w


def main():
    # Create a data structure that maps keys NESW to 1-4, respectively
    direction_handler = dict(
        (y, x) for x, y in enumerate(["N", "E", "S", "W"], start=1)
    )

    # Calculate the switch configuration required for shortest paths to each station
    out = {}
    for station, coordinates in station_coordinates.items():
        out[station] = {
            "switches": {},
            "pos": {
                "x": coordinates["x"],
                "y": coordinates["y"],
                "z": coordinates["z"],
            },
        }

        print(len(station) * 2 * "=")
        print(int(len(station) / 2) * " " + station)
        print(len(station) * 2 * "=")

        for switch in switch_coordinates.keys():
            a = nx.dijkstra_path(g, station, switch, weight)
            print(" -> ".join(list(reversed(a))))
            direction = g.edges[a[-1], a[-2]]["dir"]
            out[station]["switches"][switch] = direction_handler[direction]
        print()

    with open("sbubby.yml", "w") as out_file:
        out_file.write("---\n" + yaml.dump(out))

    with open("switch_coords.yml", "w") as coordinate_file:
        coordinate_file.write("---\n" + yaml.dump(switch_coordinates))


if __name__ == "__main__":
    main()
